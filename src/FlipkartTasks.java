//Learn to automate User Registration and Login
//Learn to automate clicking the slide-over menus with Mouse hover command
//Learn to automate scroll action using Mouse Events of Actions class
//Learn to automate Search Product
//Learn to automate Add to Cart Page to automate end-to-end functionality of Buy Product
//use : www.flipkart.com

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;

public class FlipkartTasks {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "/home/aditya/Downloads/chromedriver");

        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        //opening flipkart.com
        driver.get("https://www.flipkart.com");

//        Thread.sleep(1000);

        //closing the login pop-up dialog box
        driver.findElement(By.xpath("//button[contains(text(),'✕')]")).click(); //using relative x-path

        Thread.sleep(1000);

        mouseHoverAndClick(driver);
        searchProduct(driver);
        addToCart(driver);
//        userRegistrationAndLogin(driver);
    }

    public static void userRegistrationAndLogin(WebDriver driver){
        //Once flipkart loads we need to hover to LOGIN button
        WebElement loginBtn = driver.findElement(By.className("_1_3w1N"));

        Actions action = new Actions(driver);
        action.moveToElement(loginBtn).perform();

        driver.findElement(By.className("_3vhnxf")).click();
        driver.findElement(By.xpath("//body/div[@id='container']/div[1]/div[3]/div[1]/div[2]/div[1]/form[1]/div[1]/input[1]")).sendKeys("sample@mail.com");
        driver.findElement(By.xpath("//body[1]/div[1]/div[1]/div[3]/div[1]/div[2]/div[1]/form[1]/div[4]/button[1]/span[1]")).click();
    }

    public static void mouseHoverAndClick(WebDriver driver){
        WebElement beautyToysAndMoreBtn = driver.findElement(By.xpath("//body/div[@id='container']/div[1]/div[2]/div[1]/div[1]/div[9]/a[1]/div[2]/div[1]/div[1]"));

        //        Event of Action class
        Actions action = new Actions(driver);
        action.moveToElement(beautyToysAndMoreBtn).perform();

        WebElement booksAndMusicBtn = driver.findElement(By.xpath("//body/div[@id='container']/div[1]/div[2]/div[1]/div[1]/div[9]/a[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/a[8]"));

        action.moveToElement(booksAndMusicBtn).perform();
        WebElement nonFictionBtn = driver.findElement(By.xpath("//a[contains(text(),'Non Fiction')]"));

        action.moveToElement(nonFictionBtn).perform();
        driver.findElement(By.xpath("//a[contains(text(),'Non Fiction')]")).click();
    }

    public static void searchProduct(WebDriver driver) throws InterruptedException {
        driver.findElement(By.className("_3704LK")).sendKeys("iphone 13");
        driver.findElement(By.xpath("//body/div[@id='container']/div[1]/div[1]/div[1]/div[2]/div[2]/form[1]/div[1]/button[1]/*[1]")).click();

        Thread.sleep(1000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,800)");

        driver.findElement(By.xpath("//div[contains(text(),'APPLE iPhone 13 ((PRODUCT)RED, 128 GB)')]")).click();

    }

    public static void addToCart(WebDriver driver) throws InterruptedException {
        ArrayList<String> all = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(all.get(1));
        Thread.sleep(1000);
        driver.findElement(By.xpath("//body/div[@id='container']/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/ul[1]/li[1]/button[1]")).click();

        Thread.sleep(1000);

        driver.findElement(By.xpath("//span[contains(text(),'Place Order')]")).click();
        driver.findElement(By.xpath("//body/div[@id='container']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/input[1]")).sendKeys("sample@mail.com");
        driver.findElement(By.xpath("//span[contains(text(),'CONTINUE')]")).click(); //Steps till entering mail id
    }
}
